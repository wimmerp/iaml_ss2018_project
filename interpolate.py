"""
author: Philipp Wimmer
Interpolation between different samples.
"""

import numpy as np
import matplotlib.pyplot as plt
import random

import sklearn
import sklearn.datasets
from sklearn.model_selection import train_test_split
from sklearn import svm

import torch
import torch.utils.data
from model import VAE
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms
from torchvision.utils import save_image

expment = "results23"

model = torch.load('data/model20_deep.pt',map_location={'cuda:0': 'cpu'})

img_a = torch.from_numpy(plt.imread("data/philipp.jpg").swapaxes(0,2).swapaxes(1,2)/255.).float()
img_b = torch.from_numpy(plt.imread("data/philipp_smile.jpg").swapaxes(0,2).swapaxes(1,2)/255.).float()

print(img_a.shape)
img_a = img_a.unsqueeze(0)
img_b = img_b.unsqueeze(0)

print(img_a.shape)
n_i = 10

z_a = model.encode(img_a)[0]
z_b = model.encode(img_b)[0]

z_d = (z_b-z_a)/n_i
z_i = []
for i in range(n_i+1):
    z_i.append(z_a+i*z_d)

save_image(model.decode(z_b).view(1,3,64,64),"test.jpg")
r_i = []
for z in z_i:
    r_i.append(model.decode(z).view(1,3,64,64))

i=0
for r in r_i:
    save_image(r,"data/int"+str(i)+".jpg")
    i+=1
