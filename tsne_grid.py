import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

import sklearn
import sklearn.datasets
from sklearn.model_selection import train_test_split

import torch
import torch.utils.data
from model import VAE
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms
from torchvision.utils import save_image

import matplotlib.pyplot as plt

from MulticoreTSNE import MulticoreTSNE as TSNE


from lap import lapjv

from scipy.spatial.distance import cdist


def load_images(files):
    images = []
    for f in files:
        np_image = np.transpose(plt.imread(f),(2,0,1))#/255.
        image = torch.from_numpy(np_image)
        images.append(image)
    return torch.stack(images).float()

def prepare_data(shuffle=False):
    dset = sklearn.datasets.load_files("./data/test/",load_content=True,shuffle=shuffle)
    y = np.array(dset.target_names)[np.array(dset.target)]
    X = load_images(dset.filenames)
    return X, y

def grid_embedding(h):
    assert int(np.sqrt(h.shape[0])) ** 2 == h.shape[0], 'Nb of examples must be a square number'
    size = np.sqrt(h.shape[0])
    grid = np.dstack(np.meshgrid(np.linspace(0, 1, size), np.linspace(0, 1, size))).reshape(-1, 2)
    cost_matrix = cdist(grid, h, "sqeuclidean").astype('float32')
    cost_matrix = cost_matrix * (100000 / cost_matrix.max())
    _, rows, cols = lapjv(cost_matrix)
    return rows

def plt_tsne(X,y,size=10,recon=False,rand=False):
    model = torch.load('./results/deep_results/model30.pt',map_location={'cuda:0': 'cpu'})
    model.eval()
    with torch.no_grad():
        X = X[0:size*size]
        print(X.shape)
        z = model.encode(X)[0]
        print(z.shape)
        if(recon):
            if(rand):
                z = torch.randn(size*size, 64)
                X = model.decode(z).view(size*size,3,64,64)
        z = z.detach().numpy()
        #z = model.encode(X)[0].detach().numpy()
        print("Encoded")
        h = TSNE(n_components=2,n_jobs=8).fit_transform(z)
        print("Embedded")
        rows = grid_embedding(h)
        print("Aligned")
        print(rows.shape)
        X = X[rows]
        print(X.shape)
        X = X.reshape((size,size,3,64,64))
        print(X.shape)
        X = np.transpose(X,(0,3,1,4,2))
        print(X.shape)
        X = X.reshape((size*64,size*64,3))#*255
        plt.imsave('tsne.png',X)
        print(X.shape)

    #imsave('out.png', X)
        #plt.show()

X, y = prepare_data(False)
print(X.shape)
plt_tsne(X,y,50,True,False)
