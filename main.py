import numpy as np
import matplotlib.pyplot as plt
import os
import shutil

import argparse
import torch
import torch.utils.data
from model import VAE
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms
from torchvision.utils import save_image
from torch.utils.data.dataset import random_split

from perceptual import LossNetwork
from torchvision.models import vgg

parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--beta', type=int, default=1, metavar='B',
                        help='beta hyperparameter (default: 1)')
parser.add_argument('--dim_z', type=int, default=64, metavar='Z',
                        help='dimension of latent representation (default: 64)')
parser.add_argument('--rdir', type=str, default="results", metavar='Z',
                        help='Destionation for results (default: results)')
parser.add_argument('--batch_size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
parser.add_argument('--lr', type=float, default=0.001, metavar='LR',
                        help='learning rate (default: 0.001)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
parser.add_argument('--log_interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
parser.add_argument('--save_interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before saving the model')
parser.add_argument('--recon_interval', type=int, default=1, metavar='N',
                        help='how many batches to wait before saving reconstruction')
parser.add_argument('--loss', type=str, default="kl",
                        help='which type of loss. either rec or dfc ')
parser.add_argument('--loss_depth', type=int, default=0,
                        help='if dfc loss is applied sets depth of feauture extraction, otherwise without function')

args = parser.parse_args()

if os.path.exists(args.rdir):
     shutil.rmtree(args.rdir)
os.mkdir(args.rdir)

with open(args.rdir+'/args.txt','a') as f:
    print(f)
    f.write(str(args))
    f.close()

use_cuda = not args.no_cuda and torch.cuda.is_available()
torch.manual_seed(args.seed)
device = torch.device("cuda" if use_cuda else "cpu")
kwargs = {'num_workers': 4, 'pin_memory': True} if use_cuda else {}

channels = 3

train_set = datasets.ImageFolder('data/train'
    , transform=transforms.Compose([
     transforms.ToTensor()]))

test_set = datasets.ImageFolder('data/test'
    , transform=transforms.Compose([
     transforms.ToTensor()]))

train_loader = torch.utils.data.DataLoader(
    train_set,
    batch_size=args.batch_size, shuffle=True,
    **kwargs
)

test_loader = torch.utils.data.DataLoader(
    test_set,
    batch_size=args.batch_size, shuffle=True,
    **kwargs
)


'''
train_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=True, download=True,
                       transform=transforms.Compose([
                           transforms.Grayscale(3),
                           transforms.Resize((64,64)),
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=args.batch_size, shuffle=True, **kwargs)
test_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=False, transform=transforms.Compose([
                           transforms.Grayscale(3),
                           transforms.Resize((64,64)),
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=args.batch_size, shuffle=True, **kwargs)
'''

def init_weights(m):
    if type(m) == nn.Conv2d:
        torch.nn.init.xavier_uniform(m.weight)

model = VAE(channels,args.dim_z).apply(init_weights).to(device)
vgg_model = vgg.vgg16(pretrained=True)
loss_model = LossNetwork(vgg_model)

if torch.cuda.is_available():
    vgg_model.cuda()
    loss_model.cuda()
    model.cuda()
vgg_model.eval()
loss_model.eval()

optimizer = optim.Adam(model.parameters(), lr=args.lr)

def kl_divergence(mu, logvar):
    return  -0.5*torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

def reconstruction_loss(x, recon_x):
    return F.binary_cross_entropy(recon_x,x.view(-1,channels,64,64),size_average=False)

upsample = torch.nn.UpsamplingBilinear2d((224,224))

def perceptual_loss(x, recon_x):
    x_feature = loss_model(upsample(x.view(-1,channels,64,64)))
    recon_x_feature = loss_model(upsample(recon_x.view(-1,channels,64,64)))
    err0 = F.mse_loss(x_feature[0], recon_x_feature[0])
    if args.loss_depth==0:
        return err0
    err1 = F.mse_loss(x_feature[1], recon_x_feature[1])
    if args.loss_depth==1:
        return err0+err1
    err2 = F.mse_loss(x_feature[2], recon_x_feature[2])
    if args.loss_depth==2:
        return err0+err1+err2
    err3 = F.mse_loss(x_feature[3], recon_x_feature[3])
    if args.loss_depth==3:
        return err0+err1+err2+err3

def loss_function(recon_x, x, mu, logvar):
    REC = 0
    if args.loss=='rec':
        REC = reconstruction_loss(x,recon_x)
    elif args.loss=='dfc':
        REC = 0.02*perceptual_loss(x,recon_x)

    KLD=kl_divergence(mu,logvar)
    return REC + args.beta*KLD

def train(epoch):
    model.train()
    train_loss = 0
    for batch_idx, (data, _) in enumerate(train_loader):
        data = data.to(device)
        optimizer.zero_grad()
        recon_batch, mu, logvar = model(data)
        loss = loss_function(recon_batch, data, mu, logvar)
        loss.backward()
        train_loss += loss.item()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader),
                loss.item() / len(data)))

    print('====> Epoch: {} Average loss: {:.4f}'.format(
          epoch, train_loss / len(train_loader.dataset)))

def test(epoch):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for i, (data, _) in enumerate(test_loader):
            data = data.to(device)
            recon_batch, mu, logvar = model(data)
            #test_loss += loss_function(recon_batch, data, mu, logvar).item()
            if i == 0:
                n = min(data.size(0), 8)
                comparison = torch.cat([data[:n],
                                      recon_batch.view(args.batch_size, channels, 64, 64)[:n]])
                save_image(comparison.cpu(),
                         args.rdir+'/reconstruction_' + str(epoch) + '.png', nrow=n)

    #test_loss /= len(test_loader.dataset)
    print('====> Test set loss: {:.4f}'.format(test_loss))

epochs = args.epochs

for epoch in range(1, epochs + 1):
    if epoch == 1:
        torch.save(model,args.rdir+'/model0.pt')
        test(0)
    train(epoch)
    with torch.no_grad():
        if epoch% args.save_interval == 0:
            torch.save(model,args.rdir+'/model'+str(epoch)+'.pt')
        if epoch% args.recon_interval ==0:
            test(epoch)
            sample = torch.randn(64, args.dim_z).to(device)
            sample = model.decode(sample).cpu()
            save_image(sample.view(64, channels, 64, 64),
                   args.rdir+'/sample_' + str(epoch) + '.png')
