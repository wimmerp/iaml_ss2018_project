"""
author: Philipp Wimmer
Implementation of VAE
"""

import numpy as np
import matplotlib.pyplot as plt

import torch
import torch.utils.data
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms
from torchvision.utils import save_image

class VAE(nn.Module):
    def __init__(self,channels,dim_z):
        super(VAE, self).__init__()
        self.channels = channels
        self.dim_z = dim_z
        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()
        self.softmax = nn.Softmax2d()

        self.conv1 = nn.Sequential(
            nn.Conv2d(self.channels,32,4,2,1,bias=False), #32x32
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32,32,4,2,1,bias=False),
            nn.BatchNorm2d(32),#16x16
            nn.ReLU(),
            nn.Conv2d(32,32,4,2,1,bias=False), #8x8
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32,32,4,2,1,bias=False),
            nn.BatchNorm2d(32),#4x4
            nn.ReLU()
        )

        self.fc1 = nn.Linear(512, 256)
        self.fc2 = nn.Linear(256,256)
        self.fc31 = nn.Linear(256, self.dim_z)
        self.fc32 = nn.Linear(256, self.dim_z)

        self.fc4 = nn.Linear(self.dim_z, 256)
        self.fc5 = nn.Linear(256, 256)
        self.fc6 = nn.Linear(256,512)

        self.conv2 = nn.Sequential(
            nn.ConvTranspose2d(32, 32, 4, 2, 1,bias=False), #8x8
            nn.BatchNorm2d(32),
            nn.ReLU(True),
            nn.ConvTranspose2d(32, 32, 4, 2, 1,bias=False), #16x16
            nn.BatchNorm2d(32),
            nn.ReLU(True),
            nn.ConvTranspose2d(32, 32, 4, 2, 1,bias=False), #32x32
            nn.BatchNorm2d(32),
            nn.ReLU(True),
            nn.ConvTranspose2d(32, self.channels, 4, 2, 1,bias=False) #64x64
        )

    def encode(self, x):
        x = self.conv1(x).view((-1,512))
        x = self.relu(self.fc1(x))
        x = self.relu(self.fc2(x))
        return self.fc31(x), self.fc32(x)

    def reparameterize(self, mu, logvar):
        if self.training:
            std = torch.exp(0.5*logvar)
            eps = torch.randn_like(std)
            return eps.mul(std).add_(mu)
        else:
            return mu

    def decode(self, z):
        x = self.relu(self.fc4(z))
        x = self.relu(self.fc5(x))
        x = self.relu(self.fc6(x)).view((-1,32,4,4))
        x = self.conv2(x)
        x = self.sigmoid(x)
        return x.view((-1,self.channels,64,64))

    def forward(self, x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar
