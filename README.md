1. Download Dataset from kaggle: https://www.kaggle.com/jessicali9530/celeba-dataset

2. Clone Openface Git repository:
git clone https://github.com/cmusatyalab/openface.git

3. Prepare Data (align, crop and scale):
cd openface
./util/align-dlib.py "dataset-input" align outerEyesAndNose "dataset-output" --size 64

4. Try out and have fun:
see run.sh for example of how to train vae <br />
modify predict.py for evaluation of prediction performance <br />
modify tsne_grid.py to visualize data via plt_tsne <br />
tsne.jpg contains a high resolution image of the dataset
