"""
author: Philipp Wimmer
Comparison of SVM performance using latent features from VAE and HOG
"""

import numpy as np
import matplotlib.pyplot as plt
import random

import sklearn
import sklearn.datasets
from sklearn.model_selection import train_test_split
from sklearn import svm

import torch
import torch.utils.data
from model import VAE
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms
from torchvision.utils import save_image

from skimage.feature import hog
from skimage import color

from sklearn.manifold import TSNE

def load_images(files):
    images = []
    for f in files:
        np_image = np.transpose(plt.imread(f),(2,0,1))/255.
        images.append(torch.from_numpy(np_image))
    return torch.stack(images).float()

def prepare_data():
    train_set = sklearn.datasets.load_files("data/train1/",shuffle=True)
    test_set = sklearn.datasets.load_files("data/test1/",shuffle=True)
    y_train = np.array(train_set.target_names)[np.array(train_set.target)]
    y_test = np.array(test_set.target_names)[np.array(test_set.target)]
    X_train = train_set.filenames
    X_test = test_set.filenames
    Y_test = test_set.filenames

    y_test = np.array(y_test)
    y_train = np.array(y_train)

    X_train = load_images(X_train)
    X_test = load_images(X_test)
    return X_train, X_test, y_train, y_test

def evaluate_hog(X_train, X_test, y_train, y_test):
    X_train_hog = color.rgb2gray(np.transpose(X_train.detach().numpy(),(0,2,3,1)))
    X_test_hog = color.rgb2gray(np.transpose(X_test.detach().numpy(),(0,2,3,1)))

    hog_features_train = []
    hog_features_test = []

    for image in X_train_hog:
        fd = hog(image, orientations=8, pixels_per_cell=(8,8),cells_per_block=(4, 4),block_norm= 'L2')
        hog_features_train.append(fd)
    for image in X_test_hog:
        fd = hog(image, orientations=8, pixels_per_cell=(8,8),cells_per_block=(4, 4),block_norm= 'L2')
        hog_features_test.append(fd)

    print("Hog calculated!")
    clf_hog = svm.SVC()
    clf_hog.fit(hog_features_train,y_train)
    print("SVM trained")
    y_pred_hog = clf_hog.predict(hog_features_test)
    acc = (y_pred_hog == y_test).sum().astype(float)/y_pred_hog.size
    print("Hog acc: " + str(acc))

def evaluate_vae(X_train, X_test, y_train, y_test,model_p):
    model = torch.load(model_p,map_location={'cuda:0': 'cpu'})
    X_train_vae = model.encode(X_train)[0].detach().numpy()

    clf = svm.SVC()
    clf.fit(X_train_vae,y_train)

    X_test = model.encode(X_test)[0].detach().numpy()
    y_pred = clf.predict(X_test)
    acc = (y_pred == y_test).sum().astype(float)/y_pred.size
    print(model_p + "acc:\t" + str(acc))

X_train, X_test, y_train, y_test = prepare_data()

evaluate_hog(X_train, X_test, y_train, y_test)


models = ["results/results_kl_b1/model30.pt","results/results_kl_b2/model30.pt", "results/results_kl_b16/model30.pt",
    "results/results_kl_b32/model30.pt", "results/deep_results/model30.pt", "results/shallow_results/model20.pt",
    "results/sanity_results/model30.pt"]

for model in models:
    evaluate_vae(X_train, X_test, y_train, y_test,model)
